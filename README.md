## ** Instruction of Usage **
#### [Preinstall Instructions](preinstalled.md)
#### **This Ansible Role is for running Windows Update**
#### **_Please make sure you have met the preinstalled requirements_** 
#### 1. Open the the Linux Subsystem Bash Terminal 
#### 2. Create a folder for code -  _mkdir playbook_code_
#### 3. cd to folder -  _cd role_code_
#### 4. Clone Playbook from BitBucket
#### ** git clone https://wilsoncharlesr@bitbucket.org/wilsoncharlesr/bpic_windows_update_role.git **
#### 5. Edit Ansible vault Variables (_NOTE in order to do this you will need to decrypt the ansible vault in the group_var folder_)
#### 6. run this command to decrypt the vault - _ansible-vault decrypt vault.yml_
#### 7. run this command to encrypt the vault - _ansible-vault encrypt vault.yml_
#### ** This Role is for running Windows Update **
#### 8. Run the Role from the Playbook Folder
#### _ansible-playbook -vvvv linux_appinstall.yml -i hosts --ask-vault-pass_
#### ** _NOTE The vault password is and should be changed is password_
#### _ansible-playbook -vvvv win_update.yml -i ./hosts --ask-vault-pass_

#### NOTE - This Role creates a log file in C:\temp\winupdated_ansible.txt
#### REFERENCE for Running ANSIBLE on Windows
#### https://docs.ansible.com/ansible/devel/user_guide/windows_setup.html
#### http://docs.ansible.com/ansible/latest/user_guide/index.html
#### 
#### This Role was created by Charles R. Wilson LRS. Inc 2018
